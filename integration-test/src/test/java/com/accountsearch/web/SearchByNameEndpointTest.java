package com.accountsearch.web;


import com.accountsearch.Application;
import com.accountsearch.service.builder.AccountEntityBuilder;
import com.accountsearch.service.dao.AccountDao;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static com.jayway.restassured.RestAssured.given;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {Application.class})
@WebIntegrationTest(randomPort = true)
@ActiveProfiles("stubbed")
@DirtiesContext
public class SearchByNameEndpointTest {

	@Value("${local.server.port}")
	private Integer serverPort;

	@Autowired
	private AccountDao accountDao;

	@Before
	public void setup() {
		RestAssured.port = serverPort;
		accountDao.save(new AccountEntityBuilder().name("Facebook").build());
	}

	@Test
	public void searchAccountByNameEndpointShouldReturnAccountWithTheGivenName(){
		//@formatter:on
		given()
		.when()
			.get("/search/accounts/Facebook")
		.then()
			.statusCode(HttpStatus.SC_OK)
			.contentType(ContentType.JSON);
		//@formatter:off
	}

}
