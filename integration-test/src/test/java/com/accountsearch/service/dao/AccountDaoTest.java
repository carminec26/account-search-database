package com.accountsearch.service.dao;

import com.accountsearch.Application;
import com.accountsearch.service.builder.AccountEntityBuilder;
import com.accountsearch.service.domain.AccountEntity;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.fest.assertions.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {Application.class})
@ActiveProfiles("stubbed")
@DirtiesContext
public class AccountDaoTest {

	@Autowired
	private AccountDao accountDao;

	@Before
	public void setUp(){
		accountDao.save(new AccountEntityBuilder().name("Facebook").build());
	}

	@Test
	public void shouldReturnAccountByName(){
		String name = "Facebook";
		AccountEntity result = accountDao.findByName(name);
		assertThat(result.getName()).isEqualTo(name);
	}

}