package com.accountsearch.web;

import com.accountsearch.service.dto.Account;
import com.accountsearch.service.search.SearchService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.web.servlet.MockMvc;

import javax.servlet.http.HttpServletResponse;

import static com.jayway.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

public class SearchControllerTest {

	private MockMvc mockMvc;

	private SearchService searchService;

	@Before
	public void setUp() {
		searchService = mock(SearchService.class);
		mockMvc = standaloneSetup(new SearchController(searchService)).build();
	}

	@Test
	public void shouldReturnAnAccountWithGivenName() {
		String accountName = "Twitter";

		Account response = Account.Builder.aAccount().name(accountName).build();

		when(searchService.searchByName(anyString())).thenReturn(response);

		given().
			mockMvc(mockMvc).
		when().
			get("/search/accounts/" + accountName).
		then().
			statusCode(HttpServletResponse.SC_OK).
			contentType("application/json").
			body("name", equalTo(accountName));
	}

}