package com.accountsearch.web;

import com.accountsearch.service.dto.Account;
import com.accountsearch.service.search.SearchService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("search")
public class SearchController {

	private static final Logger logger = LogManager.getLogger();

	private SearchService searchService;

	@Autowired
	public SearchController(SearchService searchService) {
		this.searchService = searchService;
	}

	@RequestMapping(value = "/accounts", method = RequestMethod.GET)
	public List<Account> search(){
		logger.info("searching accounts");
		return searchService.search();
	}

	@RequestMapping(value = "/accounts/{name}", method = RequestMethod.GET)
	public Account searchByName(@PathVariable("name") String name){
		logger.info("searching account by name");
		return searchService.searchByName(name);
	}

}
