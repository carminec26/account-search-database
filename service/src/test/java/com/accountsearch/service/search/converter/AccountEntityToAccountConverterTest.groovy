package com.accountsearch.service.search.converter
import com.accountsearch.service.builder.AccountEntityBuilder
import com.accountsearch.service.dto.Account
import spock.lang.Specification

class AccountEntityToAccountConverterTest extends Specification {

	def converter = new AccountEntityToAccountConverter()

	def "should copy all attributes"() {
		given:
		def entity = new AccountEntityBuilder().id(1L).name("Twitter").build()

		when:
		Account account =  converter.convert(entity)

		then:
		account.getId() == entity.getId()
		account.getName() == entity.getName()
	}

}
