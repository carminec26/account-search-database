package com.accountsearch.service.search;

import com.accountsearch.service.dao.AccountDao;
import com.accountsearch.service.domain.AccountEntity;
import com.accountsearch.service.dto.Account;
import com.accountsearch.service.search.converter.AccountEntityToAccountConverter;
import com.google.common.collect.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

@RunWith(MockitoJUnitRunner.class)
public class SearchServiceImplTest {

	@Mock
	private AccountDao accountDao;

	@Mock
	private AccountEntityToAccountConverter accountEntityToAccountConverter;

	@InjectMocks
	private SearchServiceImpl searchService;

	@Test
	public void shouldReturnAllAccounts(){
		AccountEntity accountEntityOne = mock(AccountEntity.class);
		AccountEntity accountEntityTwo = mock(AccountEntity.class);

		Account accountOne = Account.Builder.aAccount().id(1).name("Twitter").build();
		Account accountTwo = Account.Builder.aAccount().id(2).name("Facebook").build();

		List<AccountEntity> accounts = Lists.newArrayList(accountEntityOne, accountEntityTwo);

		given(accountDao.findAll()).willReturn(accounts);
		given(accountEntityToAccountConverter.convert(accountEntityOne)).willReturn(accountOne);
		given(accountEntityToAccountConverter.convert(accountEntityOne)).willReturn(accountTwo);

		List<Account> response = searchService.search();

		assertThat(response.size()).isEqualTo(2);
	}

	@Test
	public void shouldReturnAccountByName(){
		AccountEntity accountEntity = mock(AccountEntity.class);
		Account account = Account.Builder.aAccount().id(1).name("Twitter").build();

		given(accountDao.findByName("Twitter")).willReturn(accountEntity);
		given(accountEntityToAccountConverter.convert(accountEntity)).willReturn(account);

		Account response = searchService.searchByName("Twitter");

		assertThat(response.getName()).isEqualTo("Twitter");
	}

}