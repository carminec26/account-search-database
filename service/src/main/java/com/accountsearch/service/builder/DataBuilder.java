package com.accountsearch.service.builder;

public interface DataBuilder<T> {
	T build();
}
