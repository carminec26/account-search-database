package com.accountsearch.service.builder;

import com.accountsearch.service.domain.AccountEntity;

public class AccountEntityBuilder implements DataBuilder<AccountEntity>{

	private long id;
	private String name;

	public AccountEntityBuilder id(long id) {
		this.id = id;
		return this;
	}

	public AccountEntityBuilder name(String name) {
		this.name = name;
		return this;
	}

	@Override
	public AccountEntity build() {
		AccountEntity accountEntity = new AccountEntity();
		accountEntity.setId(id);
		accountEntity.setName(name);
		return accountEntity;
	}
}
