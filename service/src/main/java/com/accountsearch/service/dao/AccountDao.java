package com.accountsearch.service.dao;

import com.accountsearch.service.domain.AccountEntity;
import org.springframework.data.jpa.repository.JpaRepository;


public interface AccountDao extends JpaRepository<AccountEntity, Long> {
	AccountEntity findByName(String name);
}
