package com.accountsearch.service.search;

import com.accountsearch.service.builder.AccountEntityBuilder;
import com.accountsearch.service.dao.AccountDao;
import com.accountsearch.service.domain.AccountEntity;
import com.accountsearch.service.dto.Account;
import com.accountsearch.service.search.converter.AccountEntityToAccountConverter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SearchServiceImpl implements SearchService {

	private static final Logger logger = LogManager.getLogger();

	/*@Autowired
	private Client client;*/

	@Autowired
	private AccountEntityToAccountConverter accountEntityToAccountConverter;

	@Autowired
	private AccountDao accountDao;

	@PostConstruct
	public void setUp(){
		accountDao.save(new AccountEntityBuilder().name("Twitter").build());
	}

	@Override
	public List<Account> search() {
		List<Account> accounts = new ArrayList<>();
		List<AccountEntity> accountEntities =  accountDao.findAll();

		if(accountEntities != null){
			accounts = accountEntities.stream().map(accountEntityToAccountConverter::convert).collect(Collectors.toList());
		}

		return accounts;
	}

	@Override
	public Account searchByName(String name){
		logger.debug("searching by name");
		AccountEntity accountEntity = accountDao.findByName(name);
		if(accountEntity != null){
			return accountEntityToAccountConverter.convert(accountEntity);
		}
		return null;
	}

}