package com.accountsearch.service.search.converter;

import com.accountsearch.service.domain.AccountEntity;
import com.accountsearch.service.dto.Account;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class AccountEntityToAccountConverter implements Converter<AccountEntity, Account> {

	@Override
	public Account convert(AccountEntity accountEntity) {
		Account account = new Account();
		BeanUtils.copyProperties(accountEntity, account);
		return account;
	}

}
