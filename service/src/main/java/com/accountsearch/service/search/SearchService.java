package com.accountsearch.service.search;

import com.accountsearch.service.dto.Account;

import java.util.List;

public interface SearchService {
	List<Account> search();
	Account searchByName(String name);
}
