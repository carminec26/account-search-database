package com.accountsearch.service.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseFactoryBean;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;

@Configuration
@Profile("stubbed")
public class EmbeddedDataSourceConfig {

	@Bean
	public EmbeddedDatabaseFactoryBean embeddedDataSource() {
		EmbeddedDatabaseFactoryBean embeddedDatabaseFactoryBean = new EmbeddedDatabaseFactoryBean();
		embeddedDatabaseFactoryBean.setDatabaseType(EmbeddedDatabaseType.H2);
		embeddedDatabaseFactoryBean.setDatabasePopulator(new ResourceDatabasePopulator(
				new ClassPathResource("stubbed-schema.sql"),
				new ClassPathResource("stubbed-data.sql")));
		return embeddedDatabaseFactoryBean;
	}

}
